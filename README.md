# teste técnico - parte web

Esse projeto está desenvolvido em VueJS e nele é possível gerenciar (CRUD) estabelecimentos, aqui também contém a autenticação da API REST

## Pré-Requisitos
- NodeJs
- Npm

## Instalação do projeto
```
npm install
```

### Rodando o projeto
```
npm run serve
```

### Observações
Deixei publicado a API Rest em um servidor e a mesma poderá ser acessada através desse link: [https://testeapi.me2app.com.br/](https://testeapi.me2app.com.br/). 

Mas, caso queira apontar para a sua própria api local, basta alterar a BASE URL na linha 10 no arquivo `src/http/index.js`
