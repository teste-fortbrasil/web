import Vue from 'vue'
import VueResource from 'vue-resource'
import services from './services'
import interceptors from './interceptors'

Vue.use(VueResource)

const http = Vue.http

http.options.root = 'https://testeapi.me2app.com.br/'

http.interceptors.push(interceptors)

Object.keys(services).map(service => {
  services[service] = Vue.resource('', {}, services[service])
})

const setUserToken = (apitoken) => {
  http.headers.common['api-token'] = apitoken
}

const setUserId = (userid) => {
  http.headers.common['user-id'] = userid ? userid.toString() : userid
}

export default services
export { http, setUserToken, setUserId }
