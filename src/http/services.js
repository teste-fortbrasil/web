import { services as auth } from '@/modules/auth'
import { services as company } from '@/pages/home'

export default {
  auth,
  company
}
