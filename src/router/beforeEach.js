import store from '../store'

export default async (to, from, next) => {
  document.title = `${to.name} - Fortbrasil`

  const isAuthRoutes = to.name === 'login' || to.name === 'register'

  if ((to.name !== 'login' && to.name !== 'register') && !store.getters['auth/hasToken']) {
    try {
      await store.dispatch('auth/ActionCheckToken')

      next({ path: to.path })
    } catch (err) {
      next({ name: 'login' })
    }
  } else {
    if (isAuthRoutes && store.getters['auth/hasToken']) {
      next({ name: 'home' })
    } else if (isAuthRoutes && !store.getters['auth/hasToken']) {
      try {
        await store.dispatch('auth/ActionCheckToken')

        next({ name: 'home' })
      } catch (err) {
        next()
      }
    } else {
      next()
    }
  }
}
