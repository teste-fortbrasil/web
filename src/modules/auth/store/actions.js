import services from '@/http'
import * as storage from '../storage'
import * as types from './mutation-types'

export const ActionDoLogin = ({ dispatch }, payload) => {
  return services.auth.login(payload).then(res => {
    dispatch('ActionSetUser', res.data.response)
    dispatch('ActionSetToken', res.data.response.api_token)
  })
}

export const ActionRegister = ({ dispatch }, payload) => {
  return services.auth.register(payload).then(res => {
    dispatch('ActionDoLogin', { email: payload.email, password: payload.password })
  })
}

export const ActionCheckToken = ({ dispatch, state }) => {
  if (state.token) {
    return Promise.resolve(state.token)
  }

  const token = storage.getLocalToken()

  if (!token) {
    return Promise.reject(new Error('Token Inválido'))
  }

  dispatch('ActionSetToken', token)

  const user = storage.getLocalUser()

  if (!user) {
    return Promise.reject(new Error('Sessão expirada'))
  }

  dispatch('ActionSetUser', user)
}

export const ActionLoadSession = async ({ dispatch }) => {
  const promise = new Promise((resolve, reject) => {
    try {
      const { data: { response } } = services.auth.loadSession()
      dispatch('ActionSetUser', response)
      resolve()
    } catch (err) {
      dispatch('ActionSignOut')
      reject(err)
    }
  })

  return await Promise.resolve(promise)
}

export const ActionSetUser = ({ commit }, payload) => {
  storage.setLocalUser(payload)
  storage.setHeaderUserId(payload.id)
  commit(types.SET_USER, payload)
}

export const ActionSetToken = ({ commit }, payload) => {
  commit(types.SET_TOKEN, payload)
  storage.setLocalToken(payload)
  storage.setHeaderToken(payload)
}

export const ActionSignOut = ({ dispatch }) => {
  storage.setHeaderToken('')
  storage.deleteLocalUser()
  dispatch('ActionSetUser', {})
  dispatch('ActionSetToken', '')
}
