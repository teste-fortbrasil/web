export default [
  {
    name: 'login',
    path: '/login',
    component: () => import(/* webpackChunkName: "about" */ './pages/Login')
  },
  {
    name: 'register',
    path: '/register',
    component: () => import(/* webpackChunkName: "about" */ './pages/Register')
  }
]
