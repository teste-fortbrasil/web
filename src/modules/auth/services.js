export default {
  login: { method: 'post', url: 'authenticate' },
  register: { method: 'post', url: 'user' },
  loadSession: { method: 'get', url: 'user' }
}
