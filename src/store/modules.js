import { store as auth } from '@/modules/auth'
import { store as company } from '@/pages/home'

export default {
  auth,
  company
}
