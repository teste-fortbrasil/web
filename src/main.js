import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueSweetAlert2 from 'vue-sweetalert2'

import './assets/scss/app.scss'

Vue.config.productionTip = false
Vue.use(VueSweetAlert2)
Vue.component('pagination', require('laravel-vue-pagination'))

window._Vue = new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
