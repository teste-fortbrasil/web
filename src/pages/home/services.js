export default {
  listCompanies: { method: 'get', url: 'company{?page,name}' },
  updateCompany: { method: 'post', url: 'company{/id}/update' },
  createCompany: { method: 'post', url: 'company' },
  deleteCompany: { method: 'delete', url: 'company{/id}' }
}
