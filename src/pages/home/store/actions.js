import services from '@/http'
import * as types from './mutation-types'

export const ActionListCompanies = ({ commit }, payload) => {
  services.company.listCompanies(payload).then(({ data }) => {
    commit(types.SET_COMPANIES, data.response)
  })
}

export const ActionUpdateCompany = ({ dispatch }, payload) => {
  return services.company.updateCompany({ id: payload.get('id') }, payload)
}

export const ActionCreateCompany = ({ dispatch }, payload) => {
  return services.company.createCompany(payload)
}

export const ActionDeleteCompany = ({ dispatch }, payload) => {
  return services.company.deleteCompany({ id: payload })
}
